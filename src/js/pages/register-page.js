import Input from "../components/input";
class RegisterForm {
    constructor(id, method, parent) {
        // It gives a node element
        this.method = method; // ne comprend pas si seulement si je bind le this (bind pour les evenenemts)
        this.parent = document.querySelector(`#${parent}`);
        this.nodeElement = document.createElement("form");
        this.nodeElement.id = id;
        this.nodeElement.method = method;
        this.handleSubmit = this.handleSubmit.bind(this);
    }
    init() {
        this.render();
    }
    render() {
        this.parent.appendChild(this.nodeElement);
        const UsernameComponent = new Input(
            "username",
            "text",
            "Enter your name",
            "registerForm"
        );
        UsernameComponent.init();
        const PasswordComponent = new Input(
            "password",
            "password",
            "Enter your password",
            "registerForm"
        );
        PasswordComponent.init();
        this.btnSubmit();

    }
    btnSubmit() {
        let element = document.createElement("button");
        element.id = "btn";
        element.type = "submit";
        element.innerText = "Connexion";
        this.nodeElement.appendChild(element);
        element.addEventListener("click", this.handleSubmit);
    }

    handleSubmit(event) {
        document.getElementById("password").type = "text";
        const data = {
            username: document.getElementById("username").value,
            password: document.getElementById("password").value
        };

        fetch("http://localhost:7777/api/users/authenticate", {
            method: this.method, // or 'PUT'
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify(data)
        })
            .then(response => response.json())
            .then(data => {
                console.log("Success:", data);
            })
            .catch(error => {
                console.error("Error:", error);
            });

        event.preventDefault();
    }
}
export default RegisterForm;
