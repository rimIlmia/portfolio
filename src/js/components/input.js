class Input {
    constructor(id, type, placeholder, parent) {
        this.id = id;
        this.type = type;
        this.placeholder = placeholder;

        this.value = "";
        this.state = {};

        //gives a <input/>
        this.nodeElement = document.createElement("input");
        this.nodeElement.id = this.id;
        this.nodeElement.type = this.type;
        this.nodeElement.placeholder = this.placeholder;

        this.handleChange = this.handleChange.bind(this);
        this.nodeElement.addEventListener("change", this.handleChange);

        // It gives a node element
        this.parent = document.querySelector(`#${parent}`);
    }

    init() {
        this.render();
    }

    handleChange(event) {
        this.value = event.target.value;
        console.log(this.value);
        console.log(this.nodeElement);
    }

    render() {
        this.parent.appendChild(this.nodeElement);
    }
}
export default Input;