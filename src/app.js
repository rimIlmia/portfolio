
import LoginForm from "./js/pages/login-page"
class App {
    constructor(){
        this.init();
    }
    init() {
        console.log(this);
        this.render();
    }

    render() {
        const LoginPage = new LoginForm("loginForm", "post", "app");
        LoginPage.init();
       
    }
}
export default App;